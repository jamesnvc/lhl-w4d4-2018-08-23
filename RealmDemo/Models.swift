//
//  Models.swift
//  RealmDemo
//
//  Created by James Cash on 23-08-18.
//  Copyright © 2018 Occasionally Cogent. All rights reserved.
//

import UIKit
import RealmSwift

class Reminder: Object {
    @objc dynamic var id = NSUUID().uuidString
    @objc dynamic var text = ""
    @objc dynamic var created = Date()
    // in reality, I would probably recommend storing images separately in the filesystem (or in S3 or whatever) and then just assigning a fileURL property to the image location
    @objc dynamic var imageData: Data? = nil

    static override func primaryKey() -> String {
        return "id"
    }
}

class TransientReminder {
    private var persisted: Reminder

    var text: String {
        get { return self.persisted.text }
    }

    var someTransientValue: Float = 0.0

    init(id: String, realm: Realm) {
        persisted = realm.object(ofType: Reminder.self, forPrimaryKey: id)!
    }
}

func createReminder(text: String, image: UIImage?) -> Reminder {
    let reminder = Reminder()
    reminder.text = text
    if let img = image {
        reminder.imageData = UIImageJPEGRepresentation(img, 0.8)
    }
    return reminder
}

class Group: Object {
    @objc dynamic var name = ""
    let order = RealmOptional<Int>()
    let reminders = List<Reminder>()
}
