//
//  ViewController.swift
//  RealmDemo
//
//  Created by James Cash on 23-08-18.
//  Copyright © 2018 Occasionally Cogent. All rights reserved.
//

import UIKit
import RealmSwift

class ViewController: UIViewController {

    var realm: Realm!

    override func viewDidLoad() {
        super.viewDidLoad()
        realm = try! Realm()

        if let reminder = realm.objects(Reminder.self).first {
            try! realm.write {
                reminder.text += "!"
                //            realm.delete(reminder)
                //            realm.delete(realm.objects(Reminder.self))
            }
        }

        let groups = realm.objects(Group.self)
        print("There are \(groups.count) groups")
        if groups.count == 0 {
            seedDatabase()
            print("seeded")
            let groups = realm.objects(Group.self)
            print("Now groups = \(groups)")
        } else {
            print("groups = \(groups)")
        }

        let mgroups = realm.objects(Group.self).filter { (group) -> Bool in
            group.name.hasPrefix("M")
        }

        print("Groups starting with M = \(mgroups)")


    }

    func seedDatabase() {
//        let reminder1 = Reminder()
//        reminder1.text = "There is no ethical consumption under capitalism"
        let reminder1 = createReminder(text: "There is no ethical consumption under capitalism", image: nil)

        let reminder2 = Reminder()
        reminder2.text = "The history of all struggles so far are class struggles"

        let group1 = Group()
        group1.name = "Marxist reminders"
        group1.reminders.append(reminder1)
        group1.reminders.append(reminder2)

        try! realm.write {
            realm.add([reminder1 , reminder2, group1])
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

